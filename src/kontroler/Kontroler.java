package kontroler;

import java.util.ArrayList;

import broker.DBCom;
import domen.Model;
import domen.RezervniDeoModel;
import domen.Serviser;

public class Kontroler {
	
	public static Kontroler instanca;	
	
	public static Kontroler getInstanca() {
		if(instanca==null) {
			instanca=new Kontroler();
		}
		return instanca;
	}
	public ArrayList<Model> vratiSveModele(){
		ArrayList<Model>listaModela=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		listaModela=DBCom.getBroker().vratiModel();
		DBCom.getBroker().zatvoriKonekciju();		
		return listaModela;		
	}
	public void upisiNoviModel(String sifra, String model, String brand) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().upisiNoviModel(sifra, model, brand);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public boolean daliPostojiModel(String sifra) {
		boolean postoji=false;
		DBCom.getBroker().otvoriKonekciju();
		postoji=DBCom.getBroker().daliPostojiModel(sifra);
		DBCom.getBroker().zatvoriKonekciju();
		return postoji;
	}
	public void deaktivirajModel(int selModelID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().deaktivirajModel(selModelID);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public void aktivirajModel(int selModelID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().aktivirajModel(selModelID);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public ArrayList<Serviser> vratiSveServiser() {
		ArrayList<Serviser>listaServiser=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		listaServiser=DBCom.getBroker().vratiServiser();
		DBCom.getBroker().zatvoriKonekciju();
		return listaServiser;
	}
	public ArrayList<Serviser> vratiAktivanServiser() {
		ArrayList<Serviser>listaServiser=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		listaServiser=DBCom.getBroker().vratiAktivanServiser();
		DBCom.getBroker().zatvoriKonekciju();
		return listaServiser;
	}
	public boolean daliPostojiServiser(String imeServiser) {
		boolean postoji=false;
		DBCom.getBroker().otvoriKonekciju();
		postoji=DBCom.getBroker().daliPostojiServiser(imeServiser);
		DBCom.getBroker().zatvoriKonekciju();
		return postoji;
	}
	public void upisiNoviServiser(String imeServiser, int verCode) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().upisiNoviServiser(imeServiser, verCode);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public void deaktivirajServiser(int selServiserID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().deaktivirajServiser(selServiserID);
		DBCom.getBroker().zatvoriKonekciju();
		
	}
	public void aktivirajServiser(int selServiserID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().aktivirajServiser(selServiserID);
		DBCom.getBroker().zatvoriKonekciju();
		
	}
	public ArrayList<RezervniDeoModel> vratiSveRDModel() {
		ArrayList<RezervniDeoModel>listaRD=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		listaRD=DBCom.getBroker().vratiSveRDModel();
		DBCom.getBroker().zatvoriKonekciju();
		return listaRD;
	}
	public boolean daliPostojiRD(String sifraRD) {
		boolean postoji=false;
		DBCom.getBroker().otvoriKonekciju();
		postoji=DBCom.getBroker().daliPostojiRD(sifraRD);
		DBCom.getBroker().zatvoriKonekciju();
		return postoji;
	}
	public void upisiNoviRD(String sifraRD, String opisRD, String model) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().upisiNoviRD(sifraRD, opisRD, model);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public void deaktivirajRD(int selRDID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().deaktivirajRD(selRDID);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public void aktivirajRD(int selRDID) {
		DBCom.getBroker().otvoriKonekciju();
		DBCom.getBroker().aktivirajRD(selRDID);
		DBCom.getBroker().zatvoriKonekciju();		
	}
	public ArrayList<Model> vratiAktivanModel() {
		ArrayList<Model>listaModela=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		listaModela=DBCom.getBroker().vratiAktivanModel();
		DBCom.getBroker().zatvoriKonekciju();		
		return listaModela;
	}
	public ArrayList<RezervniDeoModel> vratiRDzaOdabraniModel(String odabraniModel) {
		ArrayList<RezervniDeoModel>alRD=new ArrayList<>();
		DBCom.getBroker().otvoriKonekciju();
		alRD=DBCom.getBroker().vratiRdZaOdabraniModel(odabraniModel);
		DBCom.getBroker().zatvoriKonekciju();
		return alRD;
	}
}
