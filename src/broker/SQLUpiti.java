package broker;

public interface SQLUpiti {
	
	//MODEL SQL UPITI
	public static String VRATI_MODEL="select * from model";
	
	public static String VRATI_AKTIVAN_MODEL="select * from model where aktivno=true";
	
	public static String UPISI_NOVI_MODEL="insert into model "
	+"(sifraModel, nazivModel, brandModel) values (?,?,?)";
	
	public static String DALI_POSTOJI_MODEL="select * from model where sifraModel='";
	
	public static String DEAKTIVIRAJ_MODEL="update model set aktivno=false where idModel=";
	
	public static String AKTIVIRAJ_MODEL="update model set aktivno=true where idModel=";
	
	public static String VRATI_MODEL_ID="Select idModel from model where sifraModel='";
	
	
	
	//SERVISER SQL UPITI
	public static String VRATI_SERVISER="select * from serviser";
	
	public static String VRATI_AKTIVAN_SERVISER="select * from serviser where aktivno=true";
	
	public static String UPISI_NOVI_SERVISER="insert into serviser "
			+"(imeServiser, verCode) values (?,?)";
	
	public static String DALI_POSTOJI_SERVISER="select * from serviser where imeServiser='";
	
	public static String DEAKTIVIRAJ_SERVISER="update serviser set aktivno=false where idServiser=";
	
	public static String AKTIVIRAJ_SERVISER="update serviser set aktivno=true where idServiser=";
	
	
	
	//RD SQL UPITI
	public static String VRATI_RD_MODEL="SELECT rd.idRD, rd.sifraRD, rd.opisRD, rd.idModel,"
			+" rd.aktivno, model.sifraModel FROM `rd` "
			+"inner JOIN model on model.idModel=rd.idModel where model.aktivno=1";
	
	public static String UPISI_RD_I_MODELID="insert into rd (sifraRD, opisRD, idModel) "
			+"values (?,?,?)";
	
	public static String DALI_POSTOJI_RD="select * from rd where sifraRD='";
	
	public static String DEAKTIVIRAJ_RD="update rd set aktivno=false where idRD=";
	
	public static String AKTIVIRAJ_RD="update rd set aktivno=true where idRD=";
	
	public static String RD_ZA_ODABRANI_MODEL="SELECT * FROM `rd` INNER JOIN "
			+"model on rd.idModel=model.idModel WHERE rd.aktivno=true and model.sifraModel='";

}
