package broker;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import domen.Model;
import domen.RezervniDeoModel;
import domen.Serviser;

public class DBCom implements SQLUpiti{
	public static DBCom broker;
	private Connection con;
	
	private DBCom() {
		ucitajDriver();
	}
	public static DBCom getBroker() {
		if(broker==null) {
			broker=new DBCom();
		}
		return broker;
	}

	private void ucitajDriver() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Greska pri uspostavi veze sa bazom");
			e.printStackTrace();
		}		
	}
	
	public void otvoriKonekciju() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost/reversi", "root", "");
		} catch (SQLException e) {
			System.out.println("Greska priliko otvaranja konekcije prema bazi");
			e.printStackTrace();
		}
	}
	
	public void zatvoriKonekciju() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<Model> vratiModel() {
		
		ArrayList<Model> alModel=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(VRATI_MODEL);
			while(rs.next()) {
				Model m=new Model();
				m.setIdModel(rs.getInt("idModel"));
				m.setSifraModel(rs.getString("sifraModel"));
				m.setNazivModel (rs.getString("nazivModel"));
				m.setBrandModel(rs.getString("brandModel"));
				m.setAktivanModel(rs.getBoolean("aktivno"));
				alModel.add(m);
			}			
		} catch (SQLException e) {
			System.out.println("greska vrati model!");
			e.printStackTrace();
		}		
		return alModel;		
	}
	
	public void upisiNoviModel(String sifra, String model, String brand) {
		
		try {
			PreparedStatement ps=con.prepareStatement(UPISI_NOVI_MODEL);
			ps.setString(1, sifra);
			ps.setString(2, model);
			ps.setString(3, brand);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska pri upisu novog modela");
			e.printStackTrace();
		}		
	}
	
	public boolean daliPostojiModel(String sifra) {
		boolean postoji=false;
		ResultSet rs=null;
		Statement st=null;
		try {
			st=con.createStatement();
			rs=st.executeQuery(DALI_POSTOJI_MODEL+sifra+"'");
			while(rs.next()) {
				postoji=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return postoji;
	}
	
	public void deaktivirajModel(int selModelID) {
		try {
			PreparedStatement ps=con.prepareStatement(DEAKTIVIRAJ_MODEL+selModelID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom deaktivacije modela!");
			e.printStackTrace();
		}		
	}
	
	public void aktivirajModel(int selModelID) {
		try {
			PreparedStatement ps=con.prepareStatement(AKTIVIRAJ_MODEL+selModelID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom aktivacije modela!");
			e.printStackTrace();
		}		
	}
	
	public ArrayList<Serviser> vratiServiser() {
		ArrayList<Serviser> alServiser=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(VRATI_SERVISER);
			while(rs.next()) {
				Serviser s=new Serviser();
				s.setIdServiser(rs.getInt("idServiser"));
				s.setImeServiser(rs.getString("imeServiser"));
				s.setAktivnoServiser(rs.getBoolean("aktivno"));
				s.setVerCode(rs.getInt("verCode"));				
				alServiser.add(s);
			}			
		} catch (SQLException e) {
			System.out.println("greska vrati Serviser!");
			e.printStackTrace();
		}		
		return alServiser;				
	}
	
	public ArrayList<Serviser> vratiAktivanServiser() {
		ArrayList<Serviser> alServiser=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(VRATI_AKTIVAN_SERVISER);
			while(rs.next()) {
				Serviser s=new Serviser();
				s.setIdServiser(rs.getInt("idServiser"));
				s.setImeServiser(rs.getString("imeServiser"));
				s.setAktivnoServiser(rs.getBoolean("aktivno"));
				s.setVerCode(rs.getInt("verCode"));				
				alServiser.add(s);
			}			
		} catch (SQLException e) {
			System.out.println("greska vrati Serviser!");
			e.printStackTrace();
		}		
		return alServiser;
	}
	
	
	public boolean daliPostojiServiser(String imeServiser) {
		boolean postoji=false;
		ResultSet rs=null;
		Statement st=null;
		try {
			st=con.createStatement();
			rs=st.executeQuery(DALI_POSTOJI_SERVISER+imeServiser+"'");
			while(rs.next()) {
				postoji=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return postoji;
	}
	
	public void upisiNoviServiser(String imeServiser, int verCode) {
		try {
			PreparedStatement ps=con.prepareStatement(UPISI_NOVI_SERVISER);
			ps.setString(1, imeServiser);
			ps.setInt(2, verCode);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska pri upisu novog servisera");
			e.printStackTrace();
		}		
	}
	public void deaktivirajServiser(int selServiserID) {
		try {
			PreparedStatement ps=con.prepareStatement(DEAKTIVIRAJ_SERVISER+selServiserID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom deaktivacije servisera!");
			e.printStackTrace();
		}
		
	}
	public void aktivirajServiser(int selServiserID) {
		try {
			PreparedStatement ps=con.prepareStatement(AKTIVIRAJ_SERVISER+selServiserID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom aktivacije servisera!");
			e.printStackTrace();
		}
		
	}
	public ArrayList<RezervniDeoModel> vratiSveRDModel() {
		ArrayList<RezervniDeoModel>listaRD=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(VRATI_RD_MODEL);
			while(rs.next()) {
				RezervniDeoModel rdm=new RezervniDeoModel();
				rdm.setIdRD(rs.getInt("rd.idRD"));
				rdm.setSifraRD(rs.getString("rd.sifraRD"));
				rdm.setOpisRD(rs.getString("rd.opisRD"));
				rdm.setIdModel(rs.getInt("rd.idModel"));
				rdm.setAktivnoRD(rs.getBoolean("rd.aktivno"));
				rdm.setModelRD(rs.getString("model.sifraModel"));
				listaRD.add(rdm);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		return listaRD;
	}
	public boolean daliPostojiRD(String sifraRD) {
		boolean postoji=false;
		ResultSet rs=null;
		Statement st=null;
		try {
			st=con.createStatement();
			rs=st.executeQuery(DALI_POSTOJI_RD+sifraRD+"'");
			while(rs.next()) {
				postoji=true;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return postoji;
	}
	public void upisiNoviRD(String sifraRD, String opisRD, String model) {
		ResultSet rs=null;
		Statement st=null;
		PreparedStatement ps=null;
		int idModel=0;
		try {
			con.setAutoCommit(false);
			st=con.createStatement();
			rs=st.executeQuery(VRATI_MODEL_ID+model+"'");
			while(rs.next()) {
				idModel=rs.getInt("idModel");
			}
			if(idModel==0) {
				JOptionPane.showMessageDialog(null, "Greska prilikom upisa u bazu, ne postoji zapis o modelu!");
				return;
			}
			ps=con.prepareStatement(UPISI_RD_I_MODELID);
			ps.setString(1, sifraRD);
			ps.setString(2, opisRD);
			ps.setInt(3, idModel);
			ps.execute();
		} catch (SQLException e) {
			try {
				con.rollback();
			}catch (SQLException e1){
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		try {
			con.setAutoCommit(true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JOptionPane.showMessageDialog(null, "Upis novog RD-a uspesan");
	}
	public void deaktivirajRD(int selRDID) {
		try {
			PreparedStatement ps=con.prepareStatement(DEAKTIVIRAJ_RD+selRDID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom deaktivacije RD!");
			e.printStackTrace();
		}		
	}
	public void aktivirajRD(int selRDID) {
		try {
			PreparedStatement ps=con.prepareStatement(AKTIVIRAJ_RD+selRDID);
			ps.execute();
		} catch (SQLException e) {
			System.out.println("greska prilikom deaktivacije RD!");
			e.printStackTrace();
		}
		
	}
	public ArrayList<Model> vratiAktivanModel() {
		ArrayList<Model> alModel=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(VRATI_AKTIVAN_MODEL);
			while(rs.next()) {
				Model m=new Model();
				m.setIdModel(rs.getInt("idModel"));
				m.setSifraModel(rs.getString("sifraModel"));
				m.setNazivModel (rs.getString("nazivModel"));
				m.setBrandModel(rs.getString("brandModel"));
				m.setAktivanModel(rs.getBoolean("aktivno"));
				alModel.add(m);
			}			
		} catch (SQLException e) {
			System.out.println("greska vrati model!");
			e.printStackTrace();
		}		
		return alModel;	
	}
	public ArrayList<RezervniDeoModel> vratiRdZaOdabraniModel(String odabraniModel) {
		ArrayList<RezervniDeoModel> alRD=new ArrayList<>();
		ResultSet rs=null;
		Statement st=null;
		
		try {
			st=con.createStatement();
			rs=st.executeQuery(RD_ZA_ODABRANI_MODEL+odabraniModel+"'");
			while(rs.next()) {
				RezervniDeoModel rd=new RezervniDeoModel();
				rd.setIdRD(rs.getInt("idRD"));
				rd.setSifraRD(rs.getString("sifraRD"));
				rd.setOpisRD(rs.getString("opisRD"));
				alRD.add(rd);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return alRD;
	}
}
