package domen;

public class Model {
	private int idModel;
	private String sifraModel;
	private String nazivModel;
	private String brandModel;
	private boolean aktivanModel;
	public int getIdModel() {
		return idModel;
	}
	public void setIdModel(int idModel) {
		this.idModel = idModel;
	}
	public String getSifraModel() {
		return sifraModel;
	}
	public void setSifraModel(String sifraModel) {
		this.sifraModel = sifraModel;
	}
	public String getNazivModel() {
		return nazivModel;
	}
	public void setNazivModel(String nazivModel) {
		this.nazivModel = nazivModel;
	}
	public boolean isAktivanModel() {
		return aktivanModel;
	}
	public void setAktivanModel(boolean aktivanModel) {
		this.aktivanModel = aktivanModel;
	}
	public String getBrandModel() {
		return brandModel;
	}
	public void setBrandModel(String brandModel) {
		this.brandModel = brandModel;
	}
}
