package domen;

public class Stavke {
	
	private int idStavke;
	private int idRD;
	private int kolicina;
	private String sifraRD;
	private String opisRD;
		
	public Stavke() {
		super();
	}

	public Stavke(int idRD, int kolicina, String sifraRD, String opisRD) {
		super();
		this.idRD = idRD;
		this.kolicina = kolicina;
		this.sifraRD = sifraRD;
		this.opisRD = opisRD;
	}

	public int getIdStavke() {
		return idStavke;
	}

	public void setIdStavke(int idStavke) {
		this.idStavke = idStavke;
	}

	public int getIdRD() {
		return idRD;
	}

	public void setIdRD(int idRD) {
		this.idRD = idRD;
	}

	public int getKolicina() {
		return kolicina;
	}

	public void setKolicina(int kolicina) {
		this.kolicina = kolicina;
	}

	public String getSifraRD() {
		return sifraRD;
	}

	public void setSifraRD(String sifraRD) {
		this.sifraRD = sifraRD;
	}

	public String getOpisRD() {
		return opisRD;
	}

	public void setOpisRD(String opisRD) {
		this.opisRD = opisRD;
	}
	
	

}
