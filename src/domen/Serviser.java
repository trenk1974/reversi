package domen;

public class Serviser {
	
	private int idServiser;
	private String imeServiser;
	private int verCode;
	private boolean aktivnoServiser;
	public int getIdServiser() {
		return idServiser;
	}
	public void setIdServiser(int idServiser) {
		this.idServiser = idServiser;
	}
	public String getImeServiser() {
		return imeServiser;
	}
	public void setImeServiser(String imeServiser) {
		this.imeServiser = imeServiser;
	}
	public int getVerCode() {
		return verCode;
	}
	public void setVerCode(int verCode) {
		this.verCode = verCode;
	}
	public boolean isAktivnoServiser() {
		return aktivnoServiser;
	}
	public void setAktivnoServiser(boolean aktivnoServiser) {
		this.aktivnoServiser = aktivnoServiser;
	}

}
