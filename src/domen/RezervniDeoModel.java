package domen;

public class RezervniDeoModel {
	private int idRD;
	private String sifraRD;
	private String opisRD;
	private int idModel;
	private String modelRD;
	private boolean aktivnoRD;
	public int getIdRD() {
		return idRD;
	}
	public void setIdRD(int idRD) {
		this.idRD = idRD;
	}
	public String getSifraRD() {
		return sifraRD;
	}
	public void setSifraRD(String sifraRD) {
		this.sifraRD = sifraRD;
	}
	public String getOpisRD() {
		return opisRD;
	}
	public void setOpisRD(String opisRD) {
		this.opisRD = opisRD;
	}
	public int getIdModel() {
		return idModel;
	}
	public void setIdModel(int idModel) {
		this.idModel = idModel;
	}
	public String getModelRD() {
		return modelRD;
	}
	public void setModelRD(String modelRD) {
		this.modelRD = modelRD;
	}
	public boolean isAktivnoRD() {
		return aktivnoRD;
	}
	public void setAktivnoRD(boolean aktivnoRD) {
		this.aktivnoRD = aktivnoRD;
	}
}
