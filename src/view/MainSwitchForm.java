package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainSwitchForm extends JFrame {

	private JPanel contentPane;

	
	/**
	 * Create the frame.
	 */
	public MainSwitchForm() {
		setTitle("Reversi RD - MSF");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnAdmin = new JButton("Admin");
		btnAdmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				AdminForma af=new AdminForma();
				af.setVisible(true);
			}
		});
		btnAdmin.setBounds(10, 11, 89, 23);
		contentPane.add(btnAdmin);
		
		JLabel lblNewLabel = new JLabel("Administracija Modela, unos rezervnih delova...");
		lblNewLabel.setBounds(109, 15, 315, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnPregledZaduzenja = new JButton("Pregled");
		btnPregledZaduzenja.setBounds(10, 67, 89, 23);
		contentPane.add(btnPregledZaduzenja);
		
		JLabel lblPregledZaduzenihStavki = new JLabel("Pregled zaduzenja i razduzivanje");
		lblPregledZaduzenihStavki.setBounds(109, 71, 315, 14);
		contentPane.add(lblPregledZaduzenihStavki);
		
		JButton btnZaduzenje = new JButton("Zaduzenje");
		btnZaduzenje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ZaduzivanjeForma zf=new ZaduzivanjeForma();
				zf.setVisible(true);
			}
		});
		btnZaduzenje.setBounds(10, 129, 89, 23);
		contentPane.add(btnZaduzenje);
		
		JLabel lblRazduzivanjeZaduzenihReversa = new JLabel("Zaduzivanje RD-a");
		lblRazduzivanjeZaduzenihReversa.setBounds(109, 133, 315, 14);
		contentPane.add(lblRazduzivanjeZaduzenihReversa);
	}
}
