package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import domen.Model;
import domen.RezervniDeoModel;
import domen.Serviser;
import domen.Stavke;
import kontroler.Kontroler;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;

public class ZaduzivanjeForma extends JFrame {

	private JPanel contentPane;
	private JTextField tfKolicina;
	private JTable tableStavke;
	private ArrayList<Serviser> alServiser;
	private ArrayList<Model> alModel;
	private ArrayList<RezervniDeoModel> alRD;
	private JComboBox<String> cbServiser, cbModel, cbRD;
	private DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
	private Date datum=new Date();
	private ArrayList<Stavke> listaStavke=new ArrayList<>();
	private int idServiser;
	private int idRD;
	private String sifraRD, opisRD;
	private DefaultTableModel dtmStavke=new DefaultTableModel();
	private boolean tal=true;
	
	/**
	 * Create the frame.
	 */
	public ZaduzivanjeForma() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		cbServiser = new JComboBox();
		cbServiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String odabraniServiser[]=((String) cbServiser.getSelectedItem()).split(";");
				idServiser=Integer.parseInt(odabraniServiser[0]);
				
			}
		});
		cbServiser.setBounds(10, 11, 226, 20);
		contentPane.add(cbServiser);
		
		cbModel = new JComboBox();
		cbModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String odabraniModeliOpis[]=((String) cbModel.getSelectedItem()).split(";");
				String odabraniModel=odabraniModeliOpis[0];
				popuniComboRD(odabraniModel);
			}			
		});
		cbModel.setBounds(10, 42, 226, 20);
		contentPane.add(cbModel);
		
		cbRD = new JComboBox();
		cbRD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tal) {
					String odabraniRD[]=((String) cbRD.getSelectedItem()).split(";");
					idRD=Integer.parseInt(odabraniRD[0]);
					sifraRD=odabraniRD[1];
					opisRD=odabraniRD[2];
				}
			}
		});
		cbRD.setBounds(10, 73, 226, 20);
		contentPane.add(cbRD);
		
		tfKolicina = new JTextField();
		tfKolicina.setBounds(10, 104, 50, 20);
		contentPane.add(tfKolicina);
		tfKolicina.setColumns(10);
		
		JButton btnDodaj = new JButton("Dodaj");
		btnDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cbModel.getSelectedIndex()==0) {
					JOptionPane.showMessageDialog(null, "Odaberi model i RD");
					return;
				}
				if(cbRD.getSelectedIndex()==0) {
					JOptionPane.showMessageDialog(null, "odaberi RD");
					return;
				}
				int kolicina=0;
				try {
					kolicina=Integer.parseInt(tfKolicina.getText());
				} catch (NumberFormatException e1) {
					JOptionPane.showMessageDialog(null, "Kolicina mora biti celi broj veci od 0"
							+ "!");
					e1.printStackTrace();
					return;
				}
				if(kolicina==0) {
					JOptionPane.showMessageDialog(null, "Kolicina mora biti celi broj veci od 0");
					return;
				}
				Stavke s=new Stavke(idRD, kolicina, sifraRD, opisRD);
				listaStavke.add(s);
				popuniStavke(listaStavke);
				cbModel.setSelectedIndex(0);
				cbRD.setSelectedIndex(0);
				tfKolicina.setText("");
			}			
		});
		btnDodaj.setBounds(236, 103, 89, 23);
		contentPane.add(btnDodaj);
		
		JLabel lblNewLabel = new JLabel("Serviser");
		lblNewLabel.setBounds(246, 14, 79, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblIzaberiModel = new JLabel("Model");
		lblIzaberiModel.setBounds(246, 45, 79, 14);
		contentPane.add(lblIzaberiModel);
		
		JLabel lblIzaberiRd = new JLabel("Sifra RD");
		lblIzaberiRd.setBounds(246, 76, 79, 14);
		contentPane.add(lblIzaberiRd);
		
		JLabel lblKolicina = new JLabel("Kolicina");
		lblKolicina.setBounds(70, 107, 90, 14);
		contentPane.add(lblKolicina);
		
		JLabel lblDatum = new JLabel(df.format(datum));
		lblDatum.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblDatum.setBounds(335, 14, 89, 14);
		contentPane.add(lblDatum);
		
		JButton btnUkloni = new JButton("Ukloni");
		btnUkloni.setBounds(335, 103, 89, 23);
		contentPane.add(btnUkloni);
		
		tableStavke = new JTable(dtmStavke);
		JScrollPane scrollPane = new JScrollPane(tableStavke);
		scrollPane.setBounds(10, 135, 414, 273);
		contentPane.add(scrollPane);		
		
		scrollPane.setViewportView(tableStavke);
		
		JButton btnKnjizi = new JButton("Proknjizi");
		btnKnjizi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(idServiser==0) {
					JOptionPane.showMessageDialog(null, "IZABERI SERVISERA!!!");
					return;
				}
				
			}
		});
		btnKnjizi.setBounds(335, 419, 89, 23);
		contentPane.add(btnKnjizi);
		
		Object[]kolone=new Object[4];
		kolone[0]="idRD";
		kolone[1]="Sifra RD";
		kolone[2]="Opis RD";
		kolone[3]="kolicina";
		dtmStavke.addColumn(kolone[0]);
		dtmStavke.addColumn(kolone[1]);
		dtmStavke.addColumn(kolone[2]);
		dtmStavke.addColumn(kolone[3]);
		tableStavke.getColumnModel().getColumn(0).setMaxWidth(30);
		tableStavke.getColumnModel().getColumn(3).setMaxWidth(50);
		
		popuniComboServiser();
		popuniComboModel();
	}


	protected void popuniStavke(ArrayList<Stavke> listaStavke) {
		Object[]redovi=new Object[4];
		dtmStavke.setRowCount(0);
		for(Stavke s:listaStavke) {
			redovi[0]=s.getIdRD();
			redovi[1]=s.getSifraRD();
			redovi[2]=s.getOpisRD();
			redovi[3]=s.getKolicina();
			dtmStavke.addRow(redovi);
		}
		
	}


	private void popuniComboModel() {
		alModel=new ArrayList<>();
		alModel=Kontroler.getInstanca().vratiAktivanModel();
		cbModel.removeAllItems();
		cbModel.addItem("0;***IZABERI MODEL***");
		for(Model m:alModel) {
			cbModel.addItem(m.getSifraModel()+"; "+m.getNazivModel());
		}
		
	}


	private void popuniComboServiser() {
		alServiser=new ArrayList<>();
		alServiser=Kontroler.getInstanca().vratiAktivanServiser();
		cbServiser.removeAllItems();
		cbServiser.addItem("0;***IZABERI SERVISERA***");
		for(Serviser s:alServiser) {
			cbServiser.addItem(s.getIdServiser()+";"+s.getImeServiser());
		}
		
	}
	
	private void popuniComboRD(String odabraniModel) {
		ArrayList<RezervniDeoModel> alRD=new ArrayList<>();
		alRD=Kontroler.getInstanca().vratiRDzaOdabraniModel(odabraniModel);
		tal=false;
		cbRD.removeAllItems();
		cbRD.addItem("0;***IZABERI RD***;0");
		
		for(RezervniDeoModel rd:alRD) {
			cbRD.addItem(rd.getIdRD()+";"+rd.getSifraRD()+";"+rd.getOpisRD());
		}
		tal=true;
	}
}
