package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import domen.Model;
import domen.RezervniDeoModel;
import domen.Serviser;
import kontroler.Kontroler;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTabbedPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdminForma extends JFrame {

	private JPanel contentPane;
	private JTable tableRD;
	private JTextField tfSifraRD;
	private JTextField tfOpisRD;
	private DefaultTableModel dtmRD=new DefaultTableModel();
	private DefaultTableModel dtmModel=new DefaultTableModel();
	private DefaultTableModel dtmServiser=new DefaultTableModel();
	private JTextField tfSifraModel;
	private JTextField tfOpisModel;
	private JTable tableModel;
	private JTextField tfImeServiser;
	private JTextField tfVerCode;
	private JTable tableServiseri;
	private JComboBox cbBrand, cbModel;	
	private int selModelID, selServiserID, selRDID;

	public AdminForma() {
		setTitle("Unos podataka");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(150, 150, 600, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new LineBorder(new Color(0, 0, 0)));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 574, 450);
		contentPane.add(tabbedPane);
		
		JPanel rdTab = new JPanel();
		tabbedPane.addTab("Rezervni delovi", null, rdTab, "unos novih rezervnih delova");
		tabbedPane.setEnabledAt(0, true);
		rdTab.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Unos RD-a");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setBounds(469, 0, 90, 15);
		rdTab.add(lblNewLabel);
		
		tableRD = new JTable(dtmRD);
		tableRD.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				postaviSelektovaniIDRD();
			}
		});
		JScrollPane spRD = new JScrollPane(tableRD);
		spRD.setBounds(10, 106, 549, 292);
		rdTab.add(spRD);
				
		spRD.setViewportView(tableRD);
		
		cbModel = new JComboBox();
		cbModel.setBounds(10, 10, 166, 20);
		rdTab.add(cbModel);
		
		JLabel lblNewLabel_1 = new JLabel("Odaberi model");
		lblNewLabel_1.setBounds(186, 12, 90, 14);
		rdTab.add(lblNewLabel_1);
		
		tfSifraRD = new JTextField();
		tfSifraRD.setBounds(10, 40, 166, 20);
		rdTab.add(tfSifraRD);
		tfSifraRD.setColumns(10);
		
		tfOpisRD = new JTextField();
		tfOpisRD.setBounds(10, 71, 166, 20);
		rdTab.add(tfOpisRD);
		tfOpisRD.setColumns(10);
		
		JLabel lblUnesiSifru = new JLabel("Unesi Sifru");
		lblUnesiSifru.setBounds(186, 43, 90, 14);
		rdTab.add(lblUnesiSifru);
		
		JLabel lblUnesiOpis = new JLabel("Unesi Opis");
		lblUnesiOpis.setBounds(186, 74, 90, 14);
		rdTab.add(lblUnesiOpis);
		
		JButton btnNoviRD = new JButton("Novi RD");
		btnNoviRD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfSifraRD.getText().equals("")||tfSifraRD.getText()==null||tfOpisRD.getText().equals("")||tfOpisRD.getText()==null) {
					JOptionPane.showMessageDialog(null, "Morate popuniti polja Sifra i opis RD-a");
				}else {
					unesiNoviRD();
					cbModel.setSelectedIndex(0);
					tfSifraRD.setText("");
					tfOpisRD.setText("");
				}
			}						
		});
		btnNoviRD.setBounds(255, 72, 130, 23);
		rdTab.add(btnNoviRD);
		
		JButton btnDeaktiviraj = new JButton("Deaktiviraj RD");
		btnDeaktiviraj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().deaktivirajRD(selRDID);
				popuniTabeluRD();
			}
		});
		btnDeaktiviraj.setBounds(409, 399, 150, 23);
		rdTab.add(btnDeaktiviraj);
		
		JButton btnAktiviraRD = new JButton("Aktiviraj RD");
		btnAktiviraRD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().aktivirajRD(selRDID);
				popuniTabeluRD();
			}
		});
		btnAktiviraRD.setBounds(10, 399, 150, 23);
		rdTab.add(btnAktiviraRD);
		
		JPanel modelTab = new JPanel();
		tabbedPane.addTab("Modeli", null, modelTab, "Unos novih modela telefona");
		modelTab.setLayout(null);
		
		JLabel lblUnosModela = new JLabel("Unos Modela");
		lblUnosModela.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUnosModela.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUnosModela.setBounds(469, 0, 90, 15);
		modelTab.add(lblUnosModela);
		
		tfSifraModel = new JTextField();
		tfSifraModel.setColumns(10);
		tfSifraModel.setBounds(10, 40, 166, 20);
		modelTab.add(tfSifraModel);
		
		tfOpisModel = new JTextField();
		tfOpisModel.setColumns(10);
		tfOpisModel.setBounds(10, 71, 166, 20);
		modelTab.add(tfOpisModel);
		
		JLabel label = new JLabel("Unesi Sifru");
		label.setBounds(186, 43, 90, 14);
		modelTab.add(label);
		
		JLabel label_1 = new JLabel("Unesi Opis");
		label_1.setBounds(186, 74, 90, 14);
		modelTab.add(label_1);
		
		JButton btnNoviModel = new JButton("Novi Model");
		btnNoviModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfSifraModel.getText().equals("")||tfSifraModel.getText()==null
						||tfOpisModel.getText().equals("")||tfOpisModel.getText()==null) {
					JOptionPane.showMessageDialog(null, "Morate popuniti polja Sifra i opis uredjaja");
					
				}else {
					unesiNoviModel();
					tfSifraModel.setText("");
					tfOpisModel.setText("");					
				}
			}
		});
		btnNoviModel.setBounds(255, 72, 130, 23);
		modelTab.add(btnNoviModel);
		
		tableModel = new JTable(dtmModel);
		JScrollPane spModel = new JScrollPane(tableModel);
		tableModel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				postaviSelektovaniIDModela();
			}			
		});
		spModel.setBounds(10, 106, 549, 292);
		modelTab.add(spModel);		
		
		spModel.setViewportView(tableModel);
		
		JButton btnDeaktivirajModel = new JButton("Deaktiviraj Model");
		btnDeaktivirajModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().deaktivirajModel(selModelID);
				popuniTabeluiComboModel();
			}
		});
		btnDeaktivirajModel.setBounds(409, 399, 150, 23);
		modelTab.add(btnDeaktivirajModel);
		
		JButton btnAktivirajModel = new JButton("Aktiviraj Model");
		btnAktivirajModel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().aktivirajModel(selModelID);
				popuniTabeluiComboModel();
			}
		});
		btnAktivirajModel.setBounds(10, 399, 150, 23);
		modelTab.add(btnAktivirajModel);
		
		JLabel lblUnesiBrand = new JLabel("Odaberi Brand");
		lblUnesiBrand.setBounds(186, 12, 90, 14);
		modelTab.add(lblUnesiBrand);
		
		//za neku buducu nadogradnju u bazi napraviti tabelu "Brand" i cb popunjavati iz baze!!
		cbBrand = new JComboBox();
		cbBrand.setBounds(10, 10, 166, 20);
		modelTab.add(cbBrand);
		cbBrand.addItem("Tesla");
		cbBrand.addItem("SonyMobile");
		cbBrand.addItem("Nokia");
		
		JPanel serviserTab = new JPanel();
		tabbedPane.addTab("Serviseri", null, serviserTab, "Unos servisera");
		serviserTab.setLayout(null);
		
		JLabel lblUnosServisera = new JLabel("Unos Servisera");
		lblUnosServisera.setHorizontalAlignment(SwingConstants.RIGHT);
		lblUnosServisera.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblUnosServisera.setBounds(469, 0, 90, 15);
		serviserTab.add(lblUnosServisera);
		
		tfImeServiser = new JTextField();
		tfImeServiser.setColumns(10);
		tfImeServiser.setBounds(10, 40, 166, 20);
		serviserTab.add(tfImeServiser);
		
		tfVerCode = new JTextField();
		tfVerCode.setEnabled(false);
		tfVerCode.setEditable(false);
		tfVerCode.setColumns(10);
		tfVerCode.setBounds(10, 71, 166, 20);
		serviserTab.add(tfVerCode);
		
		JLabel lblVerifikacijskiKod = new JLabel("Ver. kod");
		lblVerifikacijskiKod.setBounds(186, 74, 90, 14);
		serviserTab.add(lblVerifikacijskiKod);
		
		JLabel lblUnesiImeI = new JLabel("Ime i prezime");
		lblUnesiImeI.setBounds(186, 43, 90, 14);
		serviserTab.add(lblUnesiImeI);
		
		JButton btnNoviServiser = new JButton("Novi Serviser");
		btnNoviServiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(tfImeServiser.getText().equals("")||tfImeServiser.getText()==null) {
					JOptionPane.showMessageDialog(null, "Morate uneti ime i prezime servisera");					
				}else {
					unesiNoviServiser();
					tfImeServiser.setText("");
					tfVerCode.setText("");					
				}
			}
		});
		btnNoviServiser.setBounds(255, 72, 130, 23);
		serviserTab.add(btnNoviServiser);
		
		tableServiseri = new JTable(dtmServiser);
		JScrollPane spServiseri = new JScrollPane(tableServiseri);
		spServiseri.setBounds(10, 106, 549, 292);
		serviserTab.add(spServiseri);
		tableServiseri.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				postaviSelektovaniIDServiser();
			}			
		});
		
		
		spServiseri.setViewportView(tableServiseri);
		
		JButton btnDeaktivirajServiser = new JButton("Deaktiviraj Servisera");
		btnDeaktivirajServiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().deaktivirajServiser(selServiserID);
				popuniTabeluServiser();
			}
		});
		btnDeaktivirajServiser.setBounds(409, 399, 150, 23);
		serviserTab.add(btnDeaktivirajServiser);
		
		JButton btnAktivirajServiser = new JButton("Aktiviraj Servisera");
		btnAktivirajServiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Kontroler.getInstanca().aktivirajServiser(selServiserID);
				popuniTabeluServiser();
			}
		});
		btnAktivirajServiser.setBounds(10, 399, 150, 23);
		serviserTab.add(btnAktivirajServiser);
		
		Object[]koloneRD=new Object[5];
		koloneRD[0]="ID";
		koloneRD[1]="Sifra_RD";
		koloneRD[2]="Opis_RD";
		koloneRD[3]="Model";
		koloneRD[4]="Aktivan";
		dtmRD.addColumn(koloneRD[0]);
		dtmRD.addColumn(koloneRD[1]);
		dtmRD.addColumn(koloneRD[2]);
		dtmRD.addColumn(koloneRD[3]);
		dtmRD.addColumn(koloneRD[4]);
		tableRD.getColumnModel().getColumn(0).setMaxWidth(30);
		tableRD.getColumnModel().getColumn(1).setMinWidth(100);
		tableRD.getColumnModel().getColumn(1).setMaxWidth(100);
		tableRD.getColumnModel().getColumn(3).setMaxWidth(80);
		tableRD.getColumnModel().getColumn(4).setMaxWidth(50);
		
		Object[]koloneModel=new Object[5];
		koloneModel[0]="ID";
		koloneModel[1]="Sifra_Model";
		koloneModel[2]="Opis_Model";
		koloneModel[3]="Brand";
		koloneModel[4]="Aktivan";
		dtmModel.addColumn(koloneModel[0]);
		dtmModel.addColumn(koloneModel[1]);
		dtmModel.addColumn(koloneModel[2]);
		dtmModel.addColumn(koloneModel[3]);
		dtmModel.addColumn(koloneModel[4]);
		tableModel.getColumnModel().getColumn(0).setMaxWidth(30);
		tableModel.getColumnModel().getColumn(1).setMaxWidth(80);
		tableModel.getColumnModel().getColumn(3).setMaxWidth(80);
		tableModel.getColumnModel().getColumn(4).setMaxWidth(50);
		
		Object[]koloneServiser=new Object[3];
		koloneServiser[0]="ID";
		koloneServiser[1]="Ime i Prezime";
		koloneServiser[2]="Aktivan";
		dtmServiser.addColumn(koloneServiser[0]);
		dtmServiser.addColumn(koloneServiser[1]);
		dtmServiser.addColumn(koloneServiser[2]);
		tableServiseri.getColumnModel().getColumn(0).setMaxWidth(30);
		tableServiseri.getColumnModel().getColumn(2).setMaxWidth(50);
		
		popuniTabeluiComboModel();
		popuniTabeluServiser();
		popuniTabeluRD();
		
	}
	
	private void popuniTabeluRD() {
		Object[]redovi=new Object[5];
		dtmRD.setRowCount(0);
		ArrayList<RezervniDeoModel>listaRDModel=new ArrayList<>();
		listaRDModel=Kontroler.getInstanca().vratiSveRDModel();
		
		for(RezervniDeoModel rdm:listaRDModel) {
			redovi[0]=rdm.getIdRD();
			redovi[1]=rdm.getSifraRD();
			redovi[2]=rdm.getOpisRD();
			redovi[3]=rdm.getModelRD();
			redovi[4]=rdm.isAktivnoRD();
			dtmRD.addRow(redovi);
		}		
	}


	private void popuniTabeluServiser() {
		Object[]redovi=new Object[3];
		dtmServiser.setRowCount(0);
		ArrayList<Serviser>listaServiser=new ArrayList<>();
		listaServiser=Kontroler.getInstanca().vratiSveServiser();
		
		for(Serviser s:listaServiser) {
			redovi[0]=s.getIdServiser();
			redovi[1]=s.getImeServiser();
			redovi[2]=s.isAktivnoServiser();
			dtmServiser.addRow(redovi);
		}		
	}

	private void popuniTabeluiComboModel() {
		Object[] redovi=new Object[5];
		dtmModel.setRowCount(0);
		cbModel.removeAllItems();
		ArrayList<Model>listaModela=new ArrayList<>();
		listaModela=Kontroler.getInstanca().vratiSveModele();
		
		for(Model m: listaModela) {
			redovi[0]=m.getIdModel();
			redovi[1]=m.getSifraModel();
			redovi[2]=m.getNazivModel();
			redovi[3]=m.getBrandModel();
			redovi[4]=m.isAktivanModel();
			dtmModel.addRow(redovi);
			
			if(m.isAktivanModel()) {
				cbModel.addItem(m.getSifraModel());
			}
		}		
	}
	
	private void unesiNoviRD() {
		String sifraRD=tfSifraRD.getText().toString();
		String opisRD=tfOpisRD.getText().toString();
		String model=cbModel.getSelectedItem().toString();
		boolean postoji=false;
		postoji=Kontroler.getInstanca().daliPostojiRD(sifraRD);
		if(!postoji) {
			Kontroler.getInstanca().upisiNoviRD(sifraRD, opisRD, model);
			popuniTabeluiComboModel();
			popuniTabeluRD();
			popuniTabeluServiser();
		}
		
	}
	
	private void unesiNoviModel() {
		
		String sifra=tfSifraModel.getText().toString();
		String model=tfOpisModel.getText().toString();
		String brand=cbBrand.getSelectedItem().toString();
		boolean postoji=false;
		postoji=Kontroler.getInstanca().daliPostojiModel(sifra);
		if(!postoji) {
			Kontroler.getInstanca().upisiNoviModel(sifra,model,brand);
			JOptionPane.showMessageDialog(null, "Novi model telefona uspesno unesen");
			popuniTabeluiComboModel();
			popuniTabeluServiser();
			popuniTabeluRD();
		}else {
			JOptionPane.showMessageDialog(null, "Model '"+sifra+"' vec postoji u bazi");
		}
	}
	
	private void unesiNoviServiser() {
		String imeServiser=tfImeServiser.getText().toString();
		int verCode=0;
		boolean postoji=false;
		postoji=Kontroler.getInstanca().daliPostojiServiser(imeServiser);
		if(!postoji) {
			try {
				verCode=Integer.parseInt(JOptionPane.showInputDialog("Serviser!! unesi verifikacijski kod!!"));
			}catch (NumberFormatException e){
				JOptionPane.showMessageDialog(null, "Verifikacijski kod mora biti broj!!");
				return;
			}
			Kontroler.getInstanca().upisiNoviServiser(imeServiser,verCode);
			JOptionPane.showMessageDialog(null, "Novi Serviser uspesno unesen!!");
			popuniTabeluiComboModel();
			popuniTabeluServiser();
			popuniTabeluRD();
		}else {
			JOptionPane.showMessageDialog(null, "Serviser '"+imeServiser+"' postoji u bazi!!");
		}
	}
	
	private void postaviSelektovaniIDModela() {
		int red=tableModel.getSelectedRow();
		selModelID=(int) (tableModel.getModel().getValueAt(red, 0));		
	}
	
	private void postaviSelektovaniIDServiser() {
		int red=tableServiseri.getSelectedRow();
		selServiserID=(int) (tableServiseri.getModel().getValueAt(red, 0));		
	}
	private void postaviSelektovaniIDRD() {
		int red=tableRD.getSelectedRow();
		selRDID=(int) (tableRD.getModel().getValueAt(red, 0));		
	}
}
